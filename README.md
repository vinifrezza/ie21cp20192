# Introdução

## Equipe

O projeto foi desenvolvido pelos alunos de Engenharia de Computação:

|Nome| gitlab user|
|---|---|
|Rodolfo Koch|rodolfokoch|
|Vinicios Frezza|vinifrezza|
|Eduardo Giodani|EduGg|

# Documentação

A documentação do projeto pode ser acessada pelo link:

> inserir o link da página de documentação.

# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)